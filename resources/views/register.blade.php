<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Halaman Register</title>
</head>

<body>

  <h1>Buat Account Baru!</h1>
  <h4>Sign Up Form</h4>
  <form action="/welcome" method="post">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="FirstName"><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="LastName"><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="Gender" value="Male">Male<br>
    <input type="radio" name="Gender" value="Female">Female<br>
    <input type="radio" name="Gender" value="Other">Other<br><br>
    <label>Nationality:</label><br>
    <select name="Nationality">
      <option value="Indonesian">Indonesian</option>
      <option value="Malaysian">Malaysian</option>
      <option value="Italian">Italian</option>
      <option value="Croatian">Croatian</option>
    </select><br><br>
    <label>Language Spoken:</label><br>
    <input type="checkbox" name="Language" value="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="Language" value="English">English<br>
    <input type="checkbox" name="Language" value="Other">Other<br><br>
    <label>Bio:</label><br>
    <textarea name="Bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="kirim">

  </form>

</body>

</html>